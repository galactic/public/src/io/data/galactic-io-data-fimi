"""
FIMI  Data reader.
"""

from collections.abc import Iterator, Mapping
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class FIMIDataReader:
    """
    `̀ FIMI`̀  Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.fimi import FIMIDataReader
    >>> reader = FIMIDataReader()
    >>> import io
    >>> data = '''1 3
    ... 2 4 5
    ... 1 2
    ... 3 4 5
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint({key: list(sorted(value)) for key, value in individuals.items()})
    {'0': ['1', '3'], '1': ['2', '4', '5'], '2': ['1', '2'], '3': ['3', '4', '5']}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``FIMI`̀  data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        individuals = []
        line = data_file.readline()
        while line:
            # reading context
            attribute_names = line.strip("\n \r\t").split(" ")
            individuals.append(frozenset(name for name in attribute_names))
            line = data_file.readline()
        return {str(index): individual for index, individual in enumerate(individuals)}

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".dat"])


def register() -> None:
    """
    Register an instance of a ``FIMI`` data reader.
    """
    PopulationFactory.register_reader(FIMIDataReader())
