"""
FIMI Data reader.
"""

from ._main import FIMIDataReader, register

__all__ = ("FIMIDataReader", "register")
