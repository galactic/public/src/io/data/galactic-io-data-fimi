"""Test population module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.fimi import FIMIDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, FIMIDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
