"""Test read module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class FIMIDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.dat"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "0": frozenset(["1", "3"]),
                "1": frozenset(["2", "4", "5"]),
                "2": frozenset(["1", "2"]),
                "3": frozenset(["3", "4", "5"]),
            },
        )
