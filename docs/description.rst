==================
*FIMI* data reader
==================

*galactic-io-data-fimi* is a data reader plugin
for **GALACTIC**.

It uses the `FIMI format <http://fimi.uantwerpen.be/>`_.

The file extension is ``.dat`` and the file format consists in describing
each individual in one line using a space separated list of attribute names.
For example:

.. code-block:: text
    :class: admonition

    c e s
    o s
    e p
    o p
    c e s
    o p
    c e
    o p
    c e
    c o s

